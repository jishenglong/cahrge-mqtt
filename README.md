# CHARGE MQTT
基于workerman的PHP异步玖鲸物联充电桩MQTT客户端。
# 安装
composer require whale-iot/charge-worker
# 文档
暂无
# 示例
**start.php**
```php
<?php
require_once __DIR__ . '/vendor/autoload.php';

use Workerman\Worker;
use ChargeWorker\BusinessWorker;
use ChargeWorker\events\Event;

$worker          = new BusinessWorker();
//worker进程名称
$worker->name    = 'BusinessWorker';
//worker进程
$worker->count   = 1;
//mqtt地址
$worker->address = 'mqtt://127.0.0.1:1883';
//mqtt参数
$worker->options = [
    'username'  => 'admin',
    'password'  => '123456',
    'client_id' => 'client_id_1',
    'debug'     => false,
];
//事件回调类
$worker->eventHandler = Event::class;

$worker->deviceFunc = function ($imei) {
    //查询设备Model
    return $imei;
};
Worker::runAll();
```
运行命令 ```php start.php start```

## EVENT
```php
//data参数
$data=[
    'imei'=>'1234567890',//设备imei
    'hex'=>['AA','BB'],//十六进制数据
    'dec'=>[1,1],//十进制数据
    'param'=>[],//解析完成的数据
    'log'=>'日志'//解析完的日志
];
//device参数是将deviceFunc闭包查询的Model类或者其他设备信息返回
```
* `Event::handle($data,$device)` 统一回调方法
* `Event::onQueryPortTotal($data,$device) ` 查询充电站端口总数（0x01）
  * ```php
     $data['param'] = [
        'port_num'   => 10,//设备端口数量
        'start_item' => 1,//第一个端口的名称
     ];
    ```
* `Event::onQueryFreePortTotal($data,$device) ` 查询所有空闲(可用)的充电站端口（0x02）
  * ```php
     $data['param'] = [
        'port_num'  => 4,//设备空闲端口数量
        'list'      => [1,2,4,7],//空闲的端口 [1,2,4,7] 1、2、4、7号端口空闲
     ];
    ```
* `Event::onQueryMoneyTotal($data,$device) ` 查询消费总额数据（0x07）
  * ```php
     $data['param'] = [
        'card_money'  => 100,//机器消费刷卡总金额（元）
        'coin_money'  => 100,//机器消费投币总金额（元）
     ];
    ```
* `Event::onIcCardAndCoinSet($data,$device) ` 设置IC卡、投币器是否可用（0x09）
  * ```php
     $data['param'] = [
        'state'  => 0,//默认0
     ];
    ```
* `Event::onDeviceFault($data,$device) ` 设备上报异常（0x0A）
  * ```php
     $data['param'] = [
        'port'          => 1,//充电端口号
        'error_code'    => 1,//错误码
     ];
    $error_code = [
        0xA0 => '停止充电异常，继电器粘连、短路',
        0xA1 => '高温异常',
        0x21 => '高温恢复正常',
        0xA2 => '低温异常',
        0x22 => '低温恢复正常',
        0xA3 => '空载，充电头脱落、充电器拔出',
        0x23 => '负载恢复正常',
        0xA4 => '消防（烟感）',
        0x24 => '消防（烟感）恢复正常',
        0xA5 => '过载',
        0x25 => '过载恢复正常',
        0xA6 => '倾斜',
        0x26 => '倾斜恢复正常',
        0xA7 => '水压高',
        0x27 => '水压（从高）恢复正常',
        0xA8 => '水压低',
        0x28 => '水压（从低）恢复正常',
        0xA9 => '过压',
        0x29 => '过压恢复正常',
        0xAA => '欠压',
        0x2A => '欠压恢复正常',
        0xAB => '预留1发生异常',
        0x2B => '预留1恢复正常',
        0xAC => '预留2发生异常',
        0x2C => '预留2恢复正常',
    ];
    ```
* `Event::onFetchTime($data,$device) ` 获取系统时间（0x0B）
  * ```php
     $data['param'] = [
        'state'  => 0,//默认0
     ];
    ```
* `Event::onPortLock($data,$device) ` 解锁或者锁定某个端口（0x0C）
  * ```php
     $data['param'] = [
        'port'  => 1,//端口号
     ];
    ```
* `Event::onRemoteStop($data,$device) ` 远程停止某个端口的充电（0x0D）
  * ```php
     $data['param'] = [
        'port'  => 1,//端口号
        'time'  => 100,//充电时间（分钟）
     ];
    ```
* `Event::onReadPortSate($data,$device) ` 读取设备每个端口的状态（0x0F）
  * ```php
     $data['param'] = [
        'port_num'  => 10,//端口数量
        'list'      => [
            'port'      =>1,//端口号    
            'status'    =>1,//端口状态 1：端口空闲；2：端口正在使用；3：端口禁用；4：端口故障。    
        ],
     ];
    ```
* `Event::onsetConfig($data,$device) ` 设置系统参数（0x18）
  * ```php
     $data['param'] = [
        'state'  => 0,//默认0
     ];
    ```