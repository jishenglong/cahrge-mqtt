<?php

namespace ChargeWorker;

use Exception;
use Workerman\Mqtt\Client;
use Workerman\Worker;

class BusinessWorker extends Worker
{
    /**
     * 端口状态字典
     */
    const PORT_STATUS = [
        0x01 => '端口空闲',
        0x02 => '端口正在使用',
        0x03 => '端口禁用',
        0x04 => '端口故障',
    ];
    /**
     * 异常字典
     */
    const ERROR_ENUM = [
        0xA0 => '停止充电异常，继电器粘连、短路',
        0xA1 => '高温异常',
        0x21 => '高温恢复正常',
        0xA2 => '低温异常',
        0x22 => '低温恢复正常',
        0xA3 => '空载，充电头脱落、充电器拔出',
        0x23 => '负载恢复正常',
        0xA4 => '消防（烟感）',
        0x24 => '消防（烟感）恢复正常',
        0xA5 => '过载',
        0x25 => '过载恢复正常',
        0xA6 => '倾斜',
        0x26 => '倾斜恢复正常',
        0xA7 => '水压高',
        0x27 => '水压（从高）恢复正常',
        0xA8 => '水压低',
        0x28 => '水压（从低）恢复正常',
        0xA9 => '过压',
        0x29 => '过压恢复正常',
        0xAA => '欠压',
        0x2A => '欠压恢复正常',
        0xAB => '预留1发生异常',
        0x2B => '预留1恢复正常',
        0xAC => '预留2发生异常',
        0x2C => '预留2恢复正常',
    ];

    const EVENT_HANDLER_ENUM = [
        0x01 => 'onQueryPortTotal',//查询充电站端口总数
        0x02 => 'onQueryFreePortTotal',//查询所有空闲(可用)的充电站端口
        0x07 => 'onQueryMoneyTotal',//查询消费总额数据
        0x09 => 'onIcCardAndCoinSet',//设置IC卡、投币器是否可用
        0x0A => 'onDeviceFault',//设备上报异常
        0x0B => 'onFetchTime',//获取系统时间
        0x0C => 'onPortLock',//解锁或者锁定某个端口
        0x0D => 'onRemoteStop',//远程停止某个端口的充电
        0x0F => 'onReadPortSate',//读取设备每个端口的状态
        0x18 => 'onsetConfig',//设置系统参数
        0x21 => 'onPortStatus',//上传端口的实时状态
    ];

    /**
     * 事件处理类，默认是 Event 类
     * @var string
     */
    public $eventHandler = 'Events';

    /**
     * 设备查询方法
     * @var null
     */
    public $deviceFunc = null;
    /**
     * mqtt地址
     * @var string
     */
    public $address = '';
    /**
     * mqtt参数
     * @var array
     * -username 用户名
     * -password 密码
     * -client_id 连接ID
     * -debug bool 调试模式
     */
    public $options = [];

    /**
     * 订阅主题
     * @var string[]
     */
    public $subscribe = [
        '/v1/device/+/rx' => 0
    ];

    /**
     * 构造函数
     *
     * @param string $socket_name
     * @param array $context_option
     */
    public function __construct($socket_name = '', array $context_option = array())
    {
        parent::__construct($socket_name, $context_option);
    }

    /**
     * 注释:运行worker
     * 创建者:JSL
     * 时间:2023/06/24 024 下午 04:03
     * @throws Exception
     */
    public function run()
    {
        $this->onWorkerStart = array($this, 'onWorkerStart');
        parent::run();
    }

    protected function onWorkerStart()
    {
        if (function_exists('opcache_reset')) {
            opcache_reset();
        }
        try {
            if (function_exists('config') && config('charge')) {
                $this->address = 'mqtt://' . config('charge.ip') . ':' . config('charge.port');
                $this->options = [
                    'username'  => config('charge.username'),
                    'password'  => config('charge.password'),
                    'client_id' => config('charge.rx_client_id'),
                    'debug'     => config('charge.debug'),
                ];
            }
            $mqtt            = new Client($this->address, $this->options);
            $mqtt->onConnect = function ($mqtt) {
                $mqtt->subscribe($this->subscribe);
            };
            $mqtt->onMessage = function ($topic, $content, $mqtt) {
                //截取imei
                preg_match('/(device\/)(.*)(?)(\/rx)/', $topic, $result);
                $imei = $result[2];
                //解析报文
                list($hex, $dec) = $this->mimeMessages($content);
                //拼接报文
                if (!$this->spliceMessage($imei, $hex, $dec)) {
                    return;
                }
                //异或校验
                if (!$this->checkXorData($dec)) {
                    return;
                }
//                var_dump(implode(' ', $hex));
                $device = null;
                if ($this->deviceFunc instanceof \Closure) {
                    $device = call_user_func($this->deviceFunc, $imei);
                }
                list($data, $functionName) = $this->setDatasource($imei, $hex, $dec);
                //统一回调
                if (is_callable($this->eventHandler . '::handle')) {
                    call_user_func($this->eventHandler . '::handle', $data, $device);
                }
                //cmd回调  不存在相应的方法 直接回调 onMessage
                if (is_callable($this->eventHandler . '::' . $functionName)) {
                    call_user_func($this->eventHandler . '::' . $functionName, $data, $device);
                }
            };
            $mqtt->connect();

        } catch (Exception $e) {
            if (is_callable($this->eventHandler . '::onError')) {
                call_user_func($this->eventHandler . '::onError', $e);
            }
        }
    }

    /**
     * 注释:解析报文
     * 创建者:JSL
     * 时间:2023/06/25 025 上午 09:52
     * @param string $message 报文原文
     * @return array
     */
    public function mimeMessages(string $message): array
    {
        //解析报文
        $message = bin2hex($message);
        //转换16进制数组
        $hex = str_split($message, 2);
        $dec = [];
        //生成十进制数组
        foreach ($hex as $value) {
            $dec[] = hexdec($value);
        }

        return [$hex, $dec];
    }

    /**
     * 注释:拼接报文
     * 创建者:JSL
     * 时间:2023/06/25 025 上午 10:28
     * @param string $imei imei
     * @param array $hex 十六进制
     * @param array $dec 十进制
     * @return bool
     */
    public function spliceMessage(string $imei, array &$hex, array &$dec): bool
    {
        global $globalCache;
        if ($dec[0] == 170 || $dec[0] == 85) {
            $len = $dec[1] ?? -2;
        } else {
            $len = -2;
        }
        if (count($dec) != $len + 2) {
            $cache = $globalCache[$imei] ?? [
                'dec'    => [],
                'hex'    => [],
                'number' => 0
            ];
            if ($cache['number'] > 10) {
                unset($globalCache[$imei]);
                return false;
            }
            $dec = array_merge($cache['dec'], $dec);
            $hex = array_merge($cache['hex'], $hex);
            $len = $dec[1];
            if (count($dec) != $len + 2) {
                $globalCache[$imei] = ['dec' => $dec, 'hex' => $hex, 'number' => $cache['number']++];
                return false;
            }
        }
        if (isset($globalCache[$imei])) {
            unset($globalCache[$imei]);
        }
        return true;
    }

    /**
     * 注释:验证异或值
     * 创建者:JSL
     * 时间:2022/10/10 9:24
     * @param array $data 十进制
     * @return bool
     */
    public function checkXorData(array $data): bool
    {
        $sum1 = $data[count($data) - 1];
        array_shift($data);
        array_pop($data);
        $sum2 = $data[0];
        array_shift($data);
        foreach ($data as $value) {
            $sum2 = $sum2 ^ $value;
        }
        return $sum2 == $sum1;
    }


    /**
     * 注释:生成业务数据
     * 创建者:JSL
     * 时间:2023/06/25 025 上午 11:18
     * @param string $imei
     * @param array $hex
     * @param array $dec
     * @return array
     */
    public function setDatasource(string $imei, array $hex, array $dec): array
    {
        $data         = [
            'imei' => $imei,
            'hex'  => $hex,
            'dec'  => $dec,
        ];
        $cmd          = $dec[2];
        $functionName = self::EVENT_HANDLER_ENUM[$cmd] ?? 'onMessage';
        $param        = [];
        $log          = '';
        switch ($cmd) {
            //查询充电站端口总数
            case 0x01:
                $param = [
                    'port_num'   => $dec[4],
                    'start_item' => $dec[5],
                ];
                $log   = [
                    '设备端口数量：' . $dec[4],
                    '第一个端口的名称：' . $dec[5],
                ];
                break;
            //查询所有空闲(可用)的充电站端口
            case 0x02:
                $port_num = $dec[4];
                $list     = [];
                if ($port_num > 0) {
                    //去掉前5个参数 然后每个端口会占用1个 取出所有端口数据 再加工
                    array_splice($dec, 0, 5);//移除前五个元素
                    array_pop($dec);
                    $list = array_chunk($dec, 1);
                }
                $param = [
                    'port_num' => $port_num,
                    'list'     => $list,
                ];
                $log   = [
                    '设备空闲端口数量：' . $port_num,
                    '空闲的端口：' . implode('、', $list)
                ];
                break;
            //查询消费总额数据
            case 0x07:
                $param = [
                    'card_money' => ($dec[4] * 256 + $dec[5]) / 10,
                    'coin_money' => ($dec[6] * 256 + $dec[6]) / 10,
                ];
                $log   = [
                    '机器消费刷卡总金额：' . $param['card_money'] . '元',
                    '机器消费投币总金额：' . $param['coin_money'] . '元'
                ];
                break;
            //设置 IC 卡、投币器是否可用
            case 0x09:
                $param = [
                    'state' => $dec[4]
                ];
                $log   = [
                    'IC卡、投币器设置成功！'
                ];
                break;
            //上传设备故障
            case 0x0A:
                $param = [
                    'port'       => $dec[4],
                    'error_code' => $dec[5],
                ];
                $error = self::ERROR_ENUM[$dec[5]] ?? '未知异常';
                $log   = [
                    '设备上报故障：' . ($dec[4] == 0xFF ? '设备' : $dec[4] . '号端口') . $error
                ];
                break;
            //获取系统时间
            case 0x0B:
                $param = [
                    'state' => $dec[4],
                ];
                $log   = [
                    '获取系统时间'
                ];
                break;
            //锁定或者解锁某一个端口
            case 0x0C:
                $param = [
                    'port' => $dec[4],
                ];
                $log   = [
                    '锁定或者解锁' . $dec['4'] . '端口'
                ];
                break;
            //远程停止某个端口的充电
            case 0x0D:
                $param = [
                    'port' => $dec[4],
                    'time' => ($dec[5] * 256 + $dec[6])
                ];
                $log   = [
                    '停止充电端口：' . $param['port'],
                    '充电时间：' . $param['time'] . '分钟',
                ];
                break;
            //读取设备每个端口的状态
            case 0x0F:
                $port_num = $dec[4];
                //去掉前5个参数 然后每个端口会占用8个 取出所有端口数据 再加工
                array_splice($dec, 0, 5);//移除前五个元素
                array_pop($dec);
                $chunk = array_chunk($dec, 2);
                $list  = [];
                foreach ($chunk as $key => $value) {
                    $list[$key] = [
                        'port'   => $value[0],
                        'status' => $value[1],
                    ];
                }
                $param = [
                    'port_num' => $port_num,
                    'list'     => $list
                ];
                $log   = [];
                foreach ($list as $key => $value) {
                    $status    = self::PORT_STATUS[$value['status']] ?? '未知状态';
                    $log[$key] = $value['port'] . '号端口：状态（' . $status . '）；';
                }
                break;
            //设置系统参数
            case 0x18:
                $param = [
                    'state' => $dec[4],
                ];
                $log   = [
                    '设置系统参数成功'
                ];
                break;
            case 0x21:
                $port_num = $dec[4];
                //去掉前5个参数 然后每个端口会占用8个 取出所有端口数据 再加工
                array_splice($dec, 0, 5);//移除前五个元素
                array_pop($dec);
                $chunk = array_chunk($dec, 8);
                $list  = [];
                foreach ($chunk as $key => $value) {
                    $list[$key] = [
                        'port'     => $value[0],
                        'status'   => $value[1],
                        'time'     => $value[2] * 256 + $value[3],
                        'power'    => $value[4] * 256 + $value[5],
                        'electric' => ($value[6] * 256 + $value[7]) / 100,
                    ];
                }
                $param = [
                    'port_num' => $port_num,
                    'list'     => $list
                ];
                $log   = [];
                foreach ($list as $key => $value) {
                    $status    = self::PORT_STATUS[$value['status']] ?? '未知状态';
                    $log[$key] = $value['port'] . '号端口：状态（' . $status . '）；时间：（' . $value['time'] . '分钟）；功率：（' . $value['power'] . 'W）；电量：（' . $value['electric'] . '度）；';
                }
                break;
            case 0xA2:
                $param = [
                    'signal' => $dec[4],
                ];
                $log   = [
                    '当前信号值：' . $dec[4]
                ];
                break;
        }
        $data['param'] = $param;
        $data['log']   = is_array($log) && count($log) > 0 ? implode("\n\r", $log) : '报文未解析';
        return [$data, $functionName];
    }
}